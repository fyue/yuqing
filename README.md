This is a project on mining public opinions from social media

FOR doc2vec settings, model files can be trained from train_file using gensim.
you can modify the doc2vec.py and call the train() function

for word2vec settings, model files are trained from corpus_bg using word2vec
    tools, which can be obtained from 
    /home/feiyue/wkdir/yuqing/scripts/tw2v.bin
    this a 200 dimension vector

Then , just run "bash runw2v.sh" if you have setup the environments.
