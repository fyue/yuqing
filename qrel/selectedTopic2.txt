<DOC><DOCNO>MB228</DOCNO><TEXT>Coumadin dietary restrictions Find information on the dietary restrictions associated with the blood thinning drug Coumadin.</TEXT></DOC>
<DOC><DOCNO>MB236</DOCNO><TEXT>California drought agricultural effects Find information on the effect of the California drought on the state's agricultural sector.</TEXT></DOC>
<DOC><DOCNO>MB242</DOCNO><TEXT>Saudi bombing Yemen Find information related to any recent bombing raids by Saudi Arabia against the Houthi of Yemen.</TEXT></DOC>
<DOC><DOCNO>MB243</DOCNO><TEXT>FIFA corruption investigation Find information related to the ongoing investigation of FIFA officials for corruption.</TEXT></DOC>
<DOC><DOCNO>MB246</DOCNO><TEXT>Greek international debt crisis Find information related to the crisis surrounding the Greek debt to international creditors, and the consequences of their possible withdrawal from the European Union.</TEXT></DOC>
<DOC><DOCNO>MB253</DOCNO><TEXT>health insurance for disabled children Find first-hand accounts of care covered by health insurance for children with disabilities, especially insurance provided through the Health Care Act.</TEXT></DOC>
<DOC><DOCNO>MB254</DOCNO><TEXT>cancer and depression Find accounts of how cancer survivors have dealt with depression after successful treatment.</TEXT></DOC>
<DOC><DOCNO>MB255</DOCNO><TEXT>medical insurance on cruises Find accounts of experiences using medical insurance while traveling on a cruise.</TEXT></DOC>
<DOC><DOCNO>MB260</DOCNO><TEXT>Society for Women and the Civil War Conference Find information on the annual conference of the Society for Women and the Civil War.</TEXT></DOC>
<DOC><DOCNO>MB262</DOCNO><TEXT>Stephen Colbert Late Show Information and opinions on David Letterman's successor on the Late Show, Stephen Colbert.</TEXT></DOC>
<DOC><DOCNO>MB265</DOCNO><TEXT>cruise ship mishaps Find tweets describing cruise ship mishaps and passengers' reactions to them.</TEXT></DOC>
<DOC><DOCNO>MB267</DOCNO><TEXT>fighting between Ukraine and pro-Russian rebels Find information related to fighting between Ukrainian government forces and pro-Russian rebels in eastern Ukraine.</TEXT></DOC>
<DOC><DOCNO>MB278</DOCNO><TEXT>"Mr. Holmes" movie Find critiques and opinions about the newly-released movie, "Mr. Holmes".</TEXT></DOC>
<DOC><DOCNO>MB284</DOCNO><TEXT>coping with identity theft Find information on consumer experiences and best practices in coping with identity theft.</TEXT></DOC>
<DOC><DOCNO>MB287</DOCNO><TEXT>"The Vatican Tapes" movie Find information on the movie "The Vatican Tapes".</TEXT></DOC>
<DOC><DOCNO>MB298</DOCNO><TEXT>Gaza rockets hit Israel Find information on rockets fired from Gaza landing in Israel.</TEXT></DOC>
<DOC><DOCNO>MB305</DOCNO><TEXT>National Museum of American History Find information about the renovated National Museum of American History in Washington, DC.</TEXT></DOC>
<DOC><DOCNO>MB324</DOCNO><TEXT>Indian-Pacific train Find opinions of the Indian-Pacific train that runs between Sydney and Perth.</TEXT></DOC>
<DOC><DOCNO>MB326</DOCNO><TEXT>wheelchair accessibility Find reports of accessibility problems for wheelchair-bound people.</TEXT></DOC>
<DOC><DOCNO>MB331</DOCNO><TEXT>Special Olympics 2015 Find information about the Special Olympics 2015 games held in Los Angeles.</TEXT></DOC>
<DOC><DOCNO>MB339</DOCNO><TEXT>Chincoteague Pony Swim Find tweets about the Annual Pony Swim in Chincoteague, MD.</TEXT></DOC>
<DOC><DOCNO>MB344</DOCNO><TEXT>Iran nuclear agreement Find tweets on developments in the negotiations to reach an agreement on limiting Iran's capacity to potentially produce nuclear weapons in exchange for relief from economic sanctions.</TEXT></DOC>
<DOC><DOCNO>MB348</DOCNO><TEXT>drones vs. commercial airliners Find information about drones flying in regions of commercial airliners.</TEXT></DOC>
<DOC><DOCNO>MB353</DOCNO><TEXT>summer Seasonal Affective Disorder (SAD) Find information on the summer version of Seasonal Affective Disorder (SAD).</TEXT></DOC>
<DOC><DOCNO>MB354</DOCNO><TEXT>"Go Set a Watchman" Find information on the publication of the book "Go Set a Watchman" by Harper Lee</TEXT></DOC>
<DOC><DOCNO>MB357</DOCNO><TEXT>prevalence of Ritalin use with no ADHD diagnosis Find information about the prevalence of Ritalin use by college students who have not been diagnosed with ADHD.</TEXT></DOC>
<DOC><DOCNO>MB359</DOCNO><TEXT>"Grey" book Find tweets about the book "Grey" by E.L. James, the fourth book in the Fifty Shades series.</TEXT></DOC>
<DOC><DOCNO>MB362</DOCNO><TEXT>Outback Steakhouse Find information on and opinions of the Outback Steakhouse restaurant chain.</TEXT></DOC>
<DOC><DOCNO>MB366</DOCNO><TEXT>climbing Mount Everest Find information and first hand accounts about climbing Mount Everest.</TEXT></DOC>
<DOC><DOCNO>MB371</DOCNO><TEXT>self-driving cars Find information on self-driving cars.</TEXT></DOC>
<DOC><DOCNO>MB377</DOCNO><TEXT>animal attacks in safari parks Find tweets relating to animals attacks on people at safari parks.</TEXT></DOC>
<DOC><DOCNO>MB379</DOCNO><TEXT>morel mushrooms Find information about morel mushrooms.</TEXT></DOC>
<DOC><DOCNO>MB383</DOCNO><TEXT>online dating for older women What online dating services are available for older women?</TEXT></DOC>
<DOC><DOCNO>MB384</DOCNO><TEXT>arson fires in inner cities Find reports of arson fires in inner cities.</TEXT></DOC>
<DOC><DOCNO>MB389</DOCNO><TEXT>Clinton Foundation Find tweets about the activities of the Clinton Foundation.</TEXT></DOC>
<DOC><DOCNO>MB391</DOCNO><TEXT>polar icecap melting Find information and opinions on reports that the icecaps are melting at both the north and south poles.</TEXT></DOC>
<DOC><DOCNO>MB392</DOCNO><TEXT>U.S. forest fires Find reports of active forest fires in the U.S.</TEXT></DOC>
<DOC><DOCNO>MB400</DOCNO><TEXT>probiotics Find information about the health benefits of probiotics.</TEXT></DOC>
<DOC><DOCNO>MB401</DOCNO><TEXT>"Knock Knock Live" What prizes were given and which celebrities appeared on Fox's "Knock Knock Live" show?</TEXT></DOC>
<DOC><DOCNO>MB405</DOCNO><TEXT>Rotterdam Unlimited What are people experiencing at Rotterdam Unlimited?</TEXT></DOC>
<DOC><DOCNO>MB409</DOCNO><TEXT>airport TSA screenings Find information on screenings by the Transportation Security Administration (TSA) at airports.</TEXT></DOC>
<DOC><DOCNO>MB416</DOCNO><TEXT>Hepworth Exhibit at the Tate Britain Return tweets about the newly-opened exhibit of Barbara Hepworth sculptures at the Tate Britain Gallery in London.</TEXT></DOC>
<DOC><DOCNO>MB419</DOCNO><TEXT>King George Weekend Ascot Find tweets about the King George Weekend at Ascot, England.</TEXT></DOC>
<DOC><DOCNO>MB432</DOCNO><TEXT>Mount Rushmore Find tweets about people's reactions to and experiences when visiting Mount Rushmore.</TEXT></DOC>
<DOC><DOCNO>MB434</DOCNO><TEXT>2015 Summer PanAm Games Find reports from the 2015 Summer PanAm Games.</TEXT></DOC>
<DOC><DOCNO>MB439</DOCNO><TEXT>Bolton Wanderers Find tweets about the Bolton Wanderers Football Club.</TEXT></DOC>
<DOC><DOCNO>MB448</DOCNO><TEXT>Bouchercon World Mystery Convention Find information on the Bouchercon World Mystery Convention.</TEXT></DOC>
