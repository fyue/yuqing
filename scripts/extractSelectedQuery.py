#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   extractSelectedQuery.py
* author: Feiyue
* date:   2015/12/14
* brief:    
* 
* updated by: null
* 
'''

import sys

try:
    query_file = sys.argv[1]
    cluster_file = sys.argv[2]
except:
    print '''
        argv[1]: query_file
        argv[2]: cluster_file
    '''
    sys.exit(1)

queries = {}
qid = ""
fi = open(query_file)
line = fi.readline()
while line:
    if "<num>" in line:
        qid = line[line.find('MB'):line.find('MB')+5]
        queries[qid] = {}
    elif '<title>' in line:
        queries[qid]['title'] = []
        line = fi.readline().strip()
        while line != '':
            queries[qid]['title'].append(line)
            line = fi.readline().strip()
        queries[qid]['title'] = ' '.join(queries[qid]['title'])
    
    elif '<desc>' in line:
        queries[qid]['desc'] = []
        line = fi.readline().strip()
        while line != '':
            queries[qid]['desc'].append(line)
            line = fi.readline().strip()
        queries[qid]['desc'] = ' '.join(queries[qid]['desc'])


    elif '<narr>' in line:
        queries[qid]['narr'] = []
        line = fi.readline().strip()
        while line != '</top>':
            queries[qid]['narr'].append(line)
            line = fi.readline().strip()
        queries[qid]['narr'] = ' '.join(queries[qid]['narr'])

    line = fi.readline()
import json
texts = open(cluster_file).readlines()
topics = list(set([ 'MB%s' % line.split(' ')[0] for line in texts]))
topics.sort()
for tp in topics:
    print "%s\t%s\t%s\t%s" %(tp, queries[tp]['title'].replace('\t', ' '),
            queries[tp]['desc'].replace('\t', ' '),
            queries[tp]['narr'].replace('\t', ' '))


