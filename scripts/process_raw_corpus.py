#! /usr/bin/python
'''
*   Copyright (C) 2016 All rights reserved.
* 
* file:   process_raw_corpus.py
* author: Feiyue
* date:   2016/01/14
* brief:    
* 
* updated by: null
* 
'''
import nltk, sys
corpus = sys.argv[1]
english_punctuations = set([',', '.', ':', ';', '?', '(', ')', '[', ']', '&',
'``', '`', '-', '...', '..',
'!', '*', '@', '#', '$', '%'])
for line in open(corpus):
    ss = line.strip().split('\t')
    try:
        words = nltk.word_tokenize(ss[2].lower())
    except:
        continue
    words = [unicode(w, 'ascii', 'ignore') for w in words if w not in
            english_punctuations]
    print '%s\t%s\t%s' % (ss[0], ' '.join(words), ss[1])
