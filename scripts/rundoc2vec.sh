#! /bin/bash
eva=doc2vec_qe.eva
>$eva
for rel in `seq 0.62 0.02 0.65`; do
    for nov in `seq 0.66 0.05 0.66`; do
        echo run_${rel}_${nov} >> $eva
        mkdir ../output/doc2vec_${rel}_${nov}
        time python doc2vec.py ../data/corpus_bg.text ../qrel/topics.ns ../data/corpus_fg.filtered.raw ../output/doc2vec_${rel}_${nov} $rel $nov 0
        python merge_output.py ../output/doc2vec_${rel}_${nov}/ ./submit/doc2vec_${rel}_${nov}
        python ../qrel/real-time-filtering-modelA-eval.py -q ../qrel/qrels.txt -c ../qrel/clusters-2015.json.txt -r ./submit/doc2vec_${rel}_${nov} >> $eva
    done
done
