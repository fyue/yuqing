#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   doc2vec.py
* author: Feiyue
* date:   2015/12/29
* brief:    
* 
* updated by: null
* 
'''
import sys, os, time, math, numpy
from gensim import corpora, models, similarities


def train_model(train_file, topic_file, doc_file):
    xtrain, xtp, xtest = [], [], []
    cnt = 1
    for line in open(train_file):
        ss = line.strip().split('\t')
        if len(ss) < 2:
            continue
        xtrain.append(models.doc2vec.LabeledSentence(
        words=ss[1].split(' '), tags=['SEN%d' % cnt]))
        cnt += 1
    for line in open(topic_file):
        ss = line.strip().split('\t')
        xtrain.append(models.doc2vec.LabeledSentence(
        words=ss[1].split(' '), tags=[ss[0]]))
    cnt = 1
    '''
    for line in open(doc_file):
        ss = line.strip().split('\t')
        xtrain.append(models.doc2vec.LabeledSentence(
        words=ss[1].split(' '), tags=['SENd%d' % cnt]))
        cnt += 1
    '''
    model_dm = models.Doc2Vec(size=300, window=8, min_count=1, workers=10,
            negative=5, alpha=0.025, min_alpha=0.025)
    #model_dbow = models.Doc2Vec(min_count=1, window=8,size=300,
            #sample=1e-3, negative=5, dm=0, workers=10, alpha=0.025,
            #min_alpha=0.025, dbow_words=1)
    print "building vocab"
    model_dm.build_vocab(xtrain)
    #model_dbow.build_vocab(xtrain)
    for epoch in range(5):
        print 'training' , epoch
        model_dm.train(xtrain)
        #model_dbow.train(xtrain)
        model_dm.alpha -= 0.0025  # decrease the learning rate
        model_dm.min_alpha = model_dm.alpha  # fix the learning rate, no decay
        #model_dbow.alpha -= 0.0025  # decrease the learning rate
        #model_dbow.min_alpha = model_dbow.alpha  # fix the learning rate, no decay

    model_dm.save('model_raw.txt')
    #model_dbow.save('model_dbow.txt')
    return model_dm

    
def cosine(v1, v2):
    len1 = numpy.linalg.norm(v1)
    len2 = numpy.linalg.norm(v2)
    sim = numpy.dot(v1, v2) / (len1 * len2)
    return 0.5 + 0.5 * sim

def fast_filter(q, d):
    ss = d.split(' ')
    wd = q.split(' ')
    for w in ss:
        if w in wd:
            return False 
    return True

def trainM(model, documents):
    model.alpha = 0.025
    model.min_alpha = 0.025
    for epoch in range(10):
        model.train(documents)
        model.alpha -= 0.0025
        model.min_alpha = model.alpha
    return model

def filterA(topic_file, doc_file, out_dir, thr_rel, thr_nov, opt, model_dm):
    #initialize topics
    topics = {}
    tp_tids= {}
    tp_vec = {}
    tp_res = {}
    tp_pool = {}
    prf = {}
    model = model_dm
    documents = []
    for line in open('qe.txt'):
        ss = line.strip().split('\t')
        sen = models.doc2vec.LabeledSentence(words=ss[1].split(' '), tags =
                [ss[0]])
        documents.append(sen)
    model = trainM(model, documents)
    for line in open(topic_file):
        ss = line.strip().split('\t')
        topics[ss[0]] = ss[1]
        tp_tids[0] = {}
        tp_vec[ss[0]] = model.docvecs[ss[0]]
        tp_res[ss[0]] = {}
        tp_pool[ss[0]] = []
        prf[ss[0]] = [tp_vec[ss[0]]]
        for day in range(20, 30):
            tp_res[ss[0]][day] = [] 
    cnt = 0
    for line in open(doc_file):
        if cnt % 10000 == 1:
            print 'processing', cnt
        cnt += 1
        ss = line.strip().split('\t')
        tid, tstr, ttime = ss[0], ss[1], ss[2]
        day = int(ttime.split(' ')[2])
        twords = tstr.split(' ')
        if len(twords) < 3:
            continue
        tvec = model.infer_vector(tstr.split(' '))
        for tp in topics:
            if fast_filter(topics[tp], tstr):
                continue
            sim_rel = cosine(tp_vec[tp], tvec)
            if sim_rel < thr_rel:
                continue
            
            #content model
            '''
            newsentence = models.doc2vec.LabeledSentence(words=twords, tags =
                    ["SEN_d%d"%cnt, tp])
            model.train([newsentence])
            tp_vec[tp] = model.docvecs[tp]
            '''
            if len(tp_res[tp][day]) > 10:
                continue
            max_sim = 0.0
            for t_ in tp_pool[tp]:
                max_sim = max(max_sim, cosine(tvec, t_))
            if max_sim > thr_nov:
                continue
            push_str = ttime.replace("+0000", "UTC")
            push_stamp = time.mktime(time.strptime(push_str,"%a %b %d %H:%M:%S %Z %Y"))
            tp_res[tp][day].append("%s %s %d content\n" % (tp, tid,
                int(push_stamp)+28800))
            tp_pool[tp].append(tvec)
    for tp in topics:
        fo =  open("%s/%s.res" % (out_dir, tp), 'w')
        for day in range(20, 30):
            fo.writelines(tp_res[tp][day])
        fo.close()
try:
    train_file = sys.argv[1]
    topic_file = sys.argv[2]
    doc_file = sys.argv[3]
    out_dir = sys.argv[4]
    thr_rel = float(sys.argv[5])
    thr_nov = float(sys.argv[6])
    opt = int(sys.argv[7])
except:
    print 'filterA.py train_file topic_file doc_file out_dir thr_rel thr_nov opt' 
    sys.exit(1)

#model = train_model(train_file, topic_file, doc_file)
filterA(topic_file, doc_file, out_dir, thr_rel, thr_nov, opt, 
        #model)
        models.Doc2Vec.load('model_raw.txt'))
