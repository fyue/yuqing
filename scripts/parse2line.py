#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   parse2line.py
* author: Feiyue
* date:   2015/12/14
* brief:    
* 
* updated by: null
* 
'''

import sys, os

try:
    parse_file = sys.argv[1]
    raw_file = sys.argv[2]
except:
    print '''
        argv[1]: parse_file
        argv[2]: raw_file
    '''
    sys.exit(1)

time_dict = {}
for line in open(raw_file):
    ss = line.strip().split('\t')
    time_dict[ss[0]] = ss[2]

texts = []
tid = ''
for line in open(parse_file):
    line = line.strip()
    if '<DOC'  in line:
        tid = line[5:-1]
    elif '</DOC>' in line:
        if len(texts) > 0:
            print '%s\t%s\t%s' % (tid, ' '.join(texts), time_dict[tid])
        texts = []
    else:
        texts.append(line)


