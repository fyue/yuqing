#! /bin/bash
>w2v.eva
for rel in `seq 0.80 0.01 0.93`;
do
    echo runw2v $rel >> w2v.eva
    python word2vec.py ../data/corpus_bg.unique ../qrel/topics.org ../data/corpus_fg.filtered ../output/w2v/ 0.89 $rel 1
    #python word2vec.py ../data/corpus_bg.unique ../qrel/topics.ns ../data/corpus_fg.filtered.raw ../output/w2v/ $rel 0.90 0
    python merge_output.py ../output/w2v/ ./submit/w2v_$rel.res
    ./single_eval.sh ./submit/w2v_$rel.res >> w2v.eva
done
