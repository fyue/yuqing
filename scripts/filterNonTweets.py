#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   filterNonTweets.py
* author: Feiyue
* date:   2015/12/18
* brief:    
* 
* updated by: null
* 
'''
import sys, os


try:
    query_file = sys.argv[1]
    corpus_file = sys.argv[2]
except:
    print '''
    query_file = sys.argv[1]
    corpus_file = sys.argv[2]
    '''
    sys.exit(1)

wdict = {}
for line in open(query_file):
    ss = line.strip().split('\t')
    for w in ss[1].split(' '):
        wdict[w] = True

for line in open(corpus_file):
    line = line.strip()
    ss = line.split('\t')
    flag = False
    for w in ss[1].split(' '):
        if w in wdict:
            flag = True
            break
    if flag:
        print line
