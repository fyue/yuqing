#! /bin/bash
>adp.eva
for rel in `seq 0.87 0.01 0.87`;
do
    echo runw2v $rel >> adp.eva
    python adp_word2vec.py ../data/corpus_bg.unique ../qrel/topics.org ../data/corpus_fg.filtered ../output/adp/ $rel 0.90 0
    #python word2vec.py ../data/corpus_bg.unique ../qrel/topics.ns ../data/corpus_fg.filtered.raw ../output/w2v/ $rel 0.90 0
    python merge_output.py ../output/adp/ ./submit/adp_$rel.res
    ./single_eval.sh ./submit/adp_$rel.res >> adp.eva
done
