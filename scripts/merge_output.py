#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   merge_output.py
* author: Feiyue
* date:   2015/12/17
* brief:    
* 
* updated by: null
* 
'''

import sys,os

out_dir =  sys.argv[1]
out_file = sys.argv[2]

files = os.listdir(out_dir)
files.sort()
files = ["%s%s" % (out_dir,f) for f in files if "res" in f]

os.system(">%s" % out_file)
for f in files:
    os.system("cat %s >> %s" % (f, out_file))
    
