#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   doc2vec.py
* author: Feiyue
* date:   2015/12/29
* brief:    
* 
* updated by: null
* 
'''
import sys, os, time, math, numpy
from gensim import corpora, models, similarities



def load_model():
    return models.Word2Vec.load_word2vec_format('tw2v.bin', binary=True) 

def cosine(v1, v2):
    sim, len1, len2 = 0.0, 0.0, 0.0
    for i in range(0, len(v1)):
        len1 += v1[i] * v1[i]
        len2 += v2[i] * v2[i]
        sim += v1[i] * v2[i]
    if len1 == 0.0 or len2 == 0.0:
        return 0.0
    return 0.5+0.5*sim / (math.sqrt(len1) * math.sqrt(len2))

def fast_filter(q, d):
    ss = d.split(' ')
    wd = q.split(' ')
    for w in ss:
        if w in wd:
            return False 
    return True

def getVec(s, model):
    a=numpy.array([model[w] for w in s.split(' ') if w in model])
    if len(a) == 0:
        return numpy.zeros(200, dtype='float64')
    return numpy.mean(a, axis=0)

def filterA(topic_file, doc_file, out_dir, thr_rel, thr_nov, opt):
    model  = load_model()    
    #initialize topics
    topics = {}
    tp_tids= {}
    tp_vec = {}
    tp_res = {}
    tp_pool = {}
    prf = {}
    for line in open(topic_file):
        ss = line.strip().split('\t')
        topics[ss[0]] = ss[1]
        tp_tids[0] = {}
        tp_vec[ss[0]] = getVec(ss[1],model)
        tp_res[ss[0]] = {}
        tp_pool[ss[0]] = []
        prf[ss[0]] = [tp_vec[ss[0]]]
        for day in range(20, 30):
            tp_res[ss[0]][day] = [] 
    cnt = 0
    for line in open(doc_file):
        if cnt % 1000000 == 0:
            print 'processing', cnt
        cnt += 1
        ss = line.strip().split('\t')
        tid, tstr, ttime = ss[0], ss[1], ss[2]
        day = int(ttime.split(' ')[2])
        twords = tstr.split(' ')
        if len(twords) < 4:
            continue
        tvec = getVec(tstr, model)
        for tp in topics:
            if fast_filter(topics[tp], tstr):
                continue
            sim_rel = cosine(tp_vec[tp], tvec)
            if sim_rel < thr_rel:
                continue
            if len(tp_res[tp][day]) > 10:
                continue
            max_sim = 0.0
            for t_ in tp_pool[tp]:
                max_sim = max(max_sim, cosine(tvec, t_))
            if max_sim > thr_nov:
                continue
            push_str = ttime.replace("+0000", "UTC")
            push_stamp = time.mktime(time.strptime(push_str,"%a %b %d %H:%M:%S %Z %Y"))
            tp_res[tp][day].append("%s %s %d content\n" % (tp, tid,
                int(push_stamp)+28800))
            tp_pool[tp].append(tvec)
    for tp in topics:
        fo =  open("%s/%s.res" % (out_dir, tp), 'w')
        for day in range(20, 30):
            fo.writelines(tp_res[tp][day])
        fo.close()
try:
    train_file = sys.argv[1]
    topic_file = sys.argv[2]
    doc_file = sys.argv[3]
    out_dir = sys.argv[4]
    thr_rel = float(sys.argv[5])
    thr_nov = float(sys.argv[6])
    opt = int(sys.argv[7])
except:
    print 'filterA.py train_file topic_file doc_file out_dir thr_rel thr_nov opt' 
    sys.exit(1)

#train_model(train_file, topic_file, doc_file)
filterA(topic_file, doc_file, out_dir, thr_rel, thr_nov, opt)
