#! /usr/bin/python
'''
*   Copyright (C) 2016 All rights reserved.
* 
* file:   generate_candidates.py
* author: Feiyue
* date:   2016/01/15
* brief:    
* 
* updated by: null
* 
'''
fi = open('../data/corpus_bg.unique')
td = {}
for line in fi.readlines():
    ss = line.strip().split('\t')
    td[ss[0]] = ss[1]

fi.close()

fi2 = open('./ret.res')
for line in fi2.readlines():
    ss = line.strip().split(' ')
    if ss[2] in td:
        print '%s\t%s' % (ss[2], td[ss[2]])
fi2.close()
