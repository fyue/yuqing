#! /usr/bin/python
'''
*   Copyright (C) 2016 All rights reserved.
* 
* file:   queryexpansion.py
* author: Feiyue
* date:   2016/01/07
* brief:    
* 
* updated by: null
* 
'''
import sys
try:
    corpus_file = sys.argv[1]
    ret_file = sys.argv[2]
except:
    print '''
    corpus_file = sys.argv[1]
    ret_file = sys.argv[2]
    '''
    sys.exit(1)
td = {}
for line in open(corpus_file):
    ss = line.strip().split('\t')
    td[ss[0]] = ss[1]
for line in open(ret_file):
    ss = line.split(' ')
    score = float(ss[4])
    if score >= 0.7 and ss[2] in td:
        print "%s\t%s" % (ss[0], td[ss[2]])

