#! /usr/bin/python # coding: utf8
import re
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   generate_corpus.py
* author: Feiyue
* date:   2015/12/04
* brief:    
* 
* updated by: null
* 
'''

import json, sys, os, gzip

gz_dir = '/index15/plain/'
bg_files, fg_files = [], []

reload(sys)
sys.setdefaultencoding('utf-8')
def xformat(x):
    if x < 10:
        return '0%d'  % x
    return '%d' % x
def extract(files,outfile):
    fo = open(outfile, 'w')
    for f in files:
        print 'processing %s' %f
        fi = gzip.open(f)
        try:
            line = fi.readline()
        except:
            continue
        while line:
            try:
                jdata = json.loads(line.strip())
            except:
                try:
                    line = fi.readline()
                    continue
                except:
                    break
                continue
            if 'delete' in jdata or 'retweeted_status' in jdata:
                try:
                    line = fi.readline()
                    continue
                except:
                    break
            tid = jdata['id_str']
            timestamp = jdata['created_at']
            if jdata.get('lang','und') == 'en': 
                jdata['text'], c = re.subn("http[^ ]*", ' ',jdata['text']+' ')

                fo.write('%s\t%s\t%s\n' % (tid, timestamp,
                jdata['text'].replace('\t', ' ').replace('\n', ' ').strip()))
            try:
                line = fi.readline()
            except:
                break
    fo.close()

for i in range(15, 20):
    for j in range(0, 24):
        bg_files.append(gz_dir+'statuses.log.2015-07-%d-%s.gz' %(i,xformat(j)))

for i in range(20, 30):
    for j in range(0, 24):
        fg_files.append(gz_dir+'statuses.log.2015-07-%d-%s.gz' %(i,xformat(j)))

#extract(bg_files, '../data/corpus_bg.raw')
extract(fg_files, '../data/corpus_fg.raw')
