#! /usr/bin/python
'''
*   Copyright (C) 2015 All rights reserved.
* 
* file:   moveDuplicates.py
* author: Feiyue
* date:   2015/12/14
* brief:    
* 
* updated by: null
* 
'''
import sys
infile = sys.argv[1]
idset = {}
for line in open(infile):
    line = line.strip()
    ss = line.split('\t')
    if ss[0] not in idset:
        print line
        idset[ss[0]] = 1
