#! /usr/bin/python
#author: ziaoang
#date: 2015/12/18

import json

INT_MAX = +99999999
INT_MIN = -99999999

content = open("../qrel/clusters-2015.json.txt").read()
a = json.loads(content)

qid_pool = a["topics"].keys()
qid_pool.sort()

topic_cluster = []
cluster_tweet = []
tid_pool = []

for qid in qid_pool:
    cluster_cnt = len(a["topics"][qid]["clusters"])
    topic_cluster.append(cluster_cnt)
    tmp = []
    for i in range(cluster_cnt):
        tweet_cnt = len(a["topics"][qid]["clusters"][i])
        tmp.append(tweet_cnt)
        tid_pool += a["topics"][qid]["clusters"][i]
    cluster_tweet.append(tmp)

print("total topic cnt: %d"%len(topic_cluster))
print("total tid cnt: %d"%len(tid_pool))
print("total distinct tid cnt: %d"%len(set(tid_pool)))

max_v = INT_MIN
min_v = INT_MAX
sum_v = 0
cnt_v = 0.0
for t in topic_cluster:
    max_v = max(max_v, t)
    min_v = min(min_v, t)
    sum_v += t
    cnt_v += 1.0
print("max cluster cnt in a topic: %d" % max_v)
print("min cluster cnt in a topic: %d" % min_v)
print("avg cluster cnt in a topic: %.2f" % (sum_v/cnt_v))

max_v = INT_MIN
min_v = INT_MAX
sum_v = 0
cnt_v = 0.0
for t in cluster_tweet:
    for tt in t:
        max_v = max(max_v, tt)
        min_v = min(min_v, tt)
        sum_v += tt
        cnt_v += 1.0
print("max tweet cnt in a cluster: %d" % max_v)
print("min tweet cnt in a cluster: %d" % min_v)
print("avg tweet cnt in a cluster: %.2f" % (sum_v/cnt_v))


tid_en_pool = []
for line in open("/home/feiyue/wkdir/yuqing/data/corpus_fg.raw"):
    t = line.strip().split("\t")
    tid_en_pool.append(t[0])

print(len(tid_en_pool))
print(len(set(tid_en_pool)))
print(len(set(tid_pool)-set(tid_en_pool)))

