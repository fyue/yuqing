/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   index.cpp
* author: Feiyue
* date:   2015/12/14
* brief:    
* 
* updated by: null
* 
*================================================*/
#include <iostream>
#include <map>
#include <unordered_map>
#include <fstream>
#include <stdlib.h>
#include <string>
#include "Index.hpp"
#include "IndexManager.hpp"
#include <exception>
#include <cmath>
#include "Utility.h"
using namespace std;
using namespace lemur::api;
using namespace lemur::parse;

#ifndef __VINDEX_CPP__
#define __VINDEX_CPP__

class VirtualIndex {
    private:
        Index *idx;
        int docNum;
        int termNum;
        unordered_map<string, int> docFreq;
        unordered_map<string, int> termFreq;
    public:
        VirtualIndex(string file) {
            unordered_map<string, int>().swap(docFreq);
            unordered_map<string, int>().swap(termFreq);
            
            ifstream fi(file.c_str());
            if (!fi) {
                cout << "FILE open error! " << file << endl;
                exit(1);
            }
            termNum = 0;
            docNum = 0;
            string line;
            while (getline(fi, line)) {
                int pos = line.find('\t');
                if (pos < 0) continue;
                vector<string> words = split(line.substr(pos+1), " ");
                for (int i = 0; i < words.size(); i ++) {
                    termFreq[words[i]] ++;
                    termNum ++;
                }
                docNum ++;
            }
            fi.close();

        }
        
        ~VirtualIndex()
        {
            unordered_map<string, int>().swap(docFreq);
            unordered_map<string, int>().swap(termFreq);
        }
        
        void update(map<string, int> & tf) {
            docNum += 1;  
            for (map<string, int>::iterator itr = tf.begin(); itr != tf.end(); itr ++) {
                termFreq[itr->first] += itr->second;
                termNum += itr->second;
            }
            return;
        }
        
        double getP(string s) {
            if (s.empty()) return 0;
            if (termFreq.find(s) == termFreq.end()) { 
                termFreq[s] = 1;
            }
            double p = (double)termFreq[s] / (double)termNum;
            
            return p;
        }

        double getIDF(string s, int v) {
            if (docFreq.find(s) == docFreq.end()) {
                docFreq[s] = 0;
            }
            double idf = log((double)docNum/(1.0+(double)docFreq[s]));
            docFreq[s] += v;
            return idf;
        }

        int getDocNum() { return docNum; }
        int getTermNum() {return termNum; }
        
};

#endif
