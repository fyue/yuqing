/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   test.cpp
* author: Feiyue
* date:   2015/12/15
* brief:    
* 
* updated by: null
* 
*================================================*/
#include <iostream>
#include <pthread.h>
#include <time.h>
#include <vector>
#include <map>
#include <stdio.h>
using namespace std;

class B {
    public:
        B() {};
        ~B() {
            cout << "destructor B " << endl;
        }
};
class A{
    private:
        vector<B> bs;
    public:
    A(B b) {
        bs.push_back(b);
    }
    ~A() {
        cout << "destructor A" << endl;
    }
};

int main() {
    map<string, int> cnt;
    cnt["0"] += 1;
    cout << cnt["0"] << endl;
    for (int i = 0; i < 5; i ++) {
        cout << i << endl;
        B b;
    }
    char str[100] = "Mon Jul 20 01:02:03 +0000 2015";
    struct tm ts;
    char tmp[4], tmp2[10];
    sscanf(str, "%*s%*s%d%d:%d:%d%*s%d",  &ts.tm_mday,&ts.tm_hour, &ts.tm_min,
            &ts.tm_sec, &ts.tm_year);
    cout << ts.tm_hour << ts.tm_mday <<  " " << ts.tm_year << " " << ts.tm_min << endl;
    ts.tm_mon = 6;
    ts.tm_year -= 1900;
    time_t stamp = mktime(&ts);
    cout << (int)mktime(&ts) + 28800<< endl;
    return 0;
}
