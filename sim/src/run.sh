#! /bin/bash

for rel in `seq 0.65 0.01 0.75`; do
    for nov in `seq 0.70 0.05 0.70`; do
        echo "running rel $rel nov $nov"
        cp ../../conf/sim.conf ../../conf/sim_${rel}_${nov}.conf
        mkdir /home/feiyue/wkdir/yuqing/output/sim_${rel}_${nov}/        
        echo "outputDir:/home/feiyue/wkdir/yuqing/output/sim_${rel}_${nov}/" >> ../../conf/sim_${rel}_$nov.conf
        time ./mainSim ../../conf/sim_${rel}_${nov}.conf $rel $nov
    done
done
