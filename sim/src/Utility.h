/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   Utility.h
* author: Feiyue
* date:   2015/12/16
* brief:    
* 
* updated by: null
* 
*================================================*/

#include <map>
#include <vector>
#include <string>
using namespace std;

#ifndef __UTILITY_H__
#define __UTILITY_H__


vector<string> split(string s, string del);
map<string, string> loadConf(char *config_file);

#endif
