/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   Utility.cpp
* author: Feiyue
* date:   2015/12/15
* brief:    
* 
* updated by: null
* 
*================================================*/

#include <iostream>
#include <vector>
#include <map>
#include <stdlib.h>
#include <string>
#include <fstream>
#include "Utility.h"
using namespace std;

#ifndef __UTILITY_CPP__
#define __UTILITY_CPP__

vector<string> split(string s, string del) {
    vector<string> res;
    if (s.size() == 0 || del.size() == 0) return res;
    int start = 0;
    while (s.size() > 0) {
        int pos = s.find(del);
        if (pos == -1) {
            res.push_back(s);
            break;
        }
        else {
            if (pos >= 0) res.push_back(s.substr(0, pos));
            s = s.substr(pos+del.size());
        }
    }
    return res;
}

map<string, string> loadConf(char * config_file) {
    map<string, string> para;
    ifstream fi(config_file);
    if (!fi) {
        cout << "open file error: " << config_file << endl;
        exit(1);
    }
    string line;
    while(getline(fi, line)) {
        int pos = line.find(":");
        if (pos == -1) break;
        para[line.substr(0, pos)] = line.substr(pos+1);
    }
    return para;
}
#endif
