/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   cluster.cpp
* author: Feiyue
* date:   2015/12/15
* brief:    
* 
* updated by: null
* 
*================================================*/
#include <vector>
#include <map>
#include <unordered_map>
#include <iostream>
#include "tweet.cpp"
#ifndef __CLUSTER_CPP__
#define __CLUSTER_CPP__

class cluster {
    private:
        vector<string> tids;
        unordered_map<string, int> tf;
        unordered_map<string, double> vec;
        int termNum;
    public:
        cluster(tweet &t) {
            tids.push_back(t.getId());
            vec.swap(t.getVec());
            tf.swap(t.getTF());
            termNum = tids.size();
        }
        ~cluster() {
            vector<string>().swap(tids);
            unordered_map<string, double>().swap(vec);
        }
        
        void addTweet(tweet &t) {
            tids.push_back(t.getId());
            termNum += t.size();
            unordered_map<string, int> tv = t.getTF();
            for (unordered_map<string,int>::iterator itr = tv.begin(); itr !=
                    tv.end(); itr ++ ) {
                if (tf.find(itr->first)!=tf.end()) {
                    tf[itr->first] += itr->second;
                }
                else tf[itr->first] = itr->second;
            }
            for (unordered_map<string, int>::iterator itr = tf.begin(); itr != tf.end();
                    itr ++) {
                vec[itr->first] = (double) itr->second / (double) termNum;
            }
            return;
        }

        int size() {
            return tids.size();
        }
        int getTermNum() {
            return termNum;
        }
        vector<string>& getTids() {
            return tids;
        }
        unordered_map<string, double> & getVec() {
            return vec;
        }
         
        void printCluster() {
            std::cout << "cluster size: " << tids.size() << endl;
            // to be fullfilled
        }
        

};



#endif
