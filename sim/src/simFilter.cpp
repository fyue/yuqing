/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   simFilter.cpp
* author: Feiyue
* date:   2015/12/15
* brief:    
* 
* updated by: null
* 
*================================================*/
#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <pthread.h>
#include "Utility.h"
#include "query.cpp"
#include "tweet.cpp"
#include "VirtualIndex.cpp"
using namespace std;

struct param {
    query *q;
    char corpusFile[100];
    char outputDir[100];
    double thr_rel;
    double thr_nov;
    VirtualIndex *vidx;

    param(query *_q, const char* _file, const char* _out, double _rel, double
            _nov, VirtualIndex *_vidx){
        q = _q;
        strcpy(corpusFile , _file);
        strcpy(outputDir, _out);
        thr_rel = _rel;
        thr_nov = _nov;
        vidx = _vidx;
    }
};

struct ans {
    vector<string> tids;
    vector<string> text;
    vector<int> timestamp;
    vector<double> relsim;
    vector<double> novsim;
    int num;
};

double computeKLSim(unordered_map<string, double>& v1, unordered_map<string, double>& v2,
        VirtualIndex * vidx, int M, int N) {

    double sim = 0.0, mu = 100.0;
    bool overlap = false;
    for (unordered_map<string, double>::iterator itr = v1.begin(); itr != v1.end(); itr++) {
        if (v2.find(itr->first) != v2.end()) {
            overlap = true;
            break; }
    }
    if (!overlap) return 0;
    for (unordered_map<string, double>::iterator itr = v1.begin(); itr != v1.end(); itr
           ++) {
       double p_Q = itr->second, p_T = 0.0, p_C = 0.0, p_D = 0.0;
       if (!v2.empty() && v2.find(itr->first) != v2.end()) p_T = v2[itr->first];
       p_C = vidx->getP(itr->first);
       double alpha = mu / (mu + N);
       p_D = (1-alpha)*p_T + alpha * p_C;
       sim += p_Q * log(p_D);
   }
   sim = sim / M;
   if (sim >= 0) return 1;
   else if (sim < -5) return 0;
   return (sim + 5) / 5;

}




void simFilter(map<string, string>& conf, double thr_rel, double thr_nov) {
    
    vector<query*> topics;
    vector<map<string, ans> > result; 
    ifstream fi(conf["topicFile"].c_str());
    if (!fi) {
        cout << "File open error: " << conf["topicFile"] << endl;
        exit(1);
    }
    cout << "Loading Index" << endl;
    VirtualIndex *vidx = new VirtualIndex(conf["indexPath"]);
    cout << "Load Index complete" << endl;
    //cout << vidx->getTermNum() << endl;
    //cout << vidx->getDocNum() << endl;
    string line;
    while (getline(fi, line)) {
        int pos = line.find("\t");
        string qid = line.substr(0,pos);
        string text = line.substr(pos+1);
        topics.push_back(new query(qid, text));
        result.push_back(map<string,ans>()); 
    }
    fi.close();

    ifstream cf(conf["corpusFile"].c_str());
    if (!cf) {
        cout << "FILE open error: " << conf["corpusFile"] << endl;
        exit(1);
    }
    int cnt = 0;
    while (getline(cf, line)) {
        if (++cnt % 100000 == 0) cout << cnt << endl;
        vector<string> strs = split(line, "\t");
        tweet t(strs[0], strs[1], strs[2]);
        unordered_map<string, double> tvec = t.getVec();
        int tn =  t.size();

        for (int i = 0; i < topics.size(); i ++) {
            unordered_map<string, double> qvec = topics[i]->getVec();

            double klsim = computeKLSim(qvec, tvec, vidx,
                    topics[i]->size(), tn);
            if (klsim < thr_rel) {
                continue;
            }
            //cout << t.getId() << " score: " << klsim << endl;
            vector<cluster> qc = topics[i]->getClusters();
            double maxsim = 0.0;
            int pos = -1, nc = topics[i]->getClusterNum();
            //cout << "clusters of topics" << topics[i]->getId() <<
                //": " << qc.size() << topics[i]->getClusterNum() << endl;
            for (int j = 0; j < nc; j ++) {
                double nsim = computeKLSim(tvec, qc[j].getVec(), vidx,
                        tn, qc[j].getTermNum());
                if (nsim > maxsim) {
                    maxsim = nsim;
                    pos = j;
                }
            }
            cout << klsim << '-' << maxsim <<"pos"<< pos << endl;
            if (pos >= 0 && (maxsim > thr_nov || topics[i]->getClusterNum() > 20) ) {
                topics[i]->updateCluster(pos, t); 
                //cout << topics[i]->getClusterNum() << "update" << endl;
                
            }
            else {
                topics[i]->addNewCluster(t);
                //cout << topics[i]->getClusterNum() << "new" << endl;

                //cout << "dayq" << t.getDay() << endl;
                string day = t.getDay();
                if (result[i].find(day) == result[i].end()) {
                    ans ta;
                    ta.num = 0; 
                    result[i][day] = ta;
                }
                if (result[i][day].num < 10) {
                    result[i][day].tids.push_back(t.getId());
                    result[i][day].text.push_back(t.getText());
                    result[i][day].relsim.push_back(klsim);
                    result[i][day].novsim.push_back(maxsim);
                    result[i][day].timestamp.push_back(t.getTimestamp());
                    result[i][day].num ++;
                }
                //cout << day << " result: " << result[day].tids[0] << endl;
            }
        }
    }
    cf.close();
    
    for (int i = 0; i < topics.size(); i ++) { 
        char outfile[100], outfile2[100];
        sprintf(outfile, "%s%s.res", conf["outputDir"].c_str(), topics[i]->getId().c_str());
        sprintf(outfile2, "%s%s.vb", conf["outputDir"].c_str(), topics[i]->getId().c_str());
        ofstream fo(outfile);
        ofstream f2(outfile2);
        if (!fo) {
            cout << "FILE open error! " << outfile << endl;
            exit(1);
        }
        stringstream sstr;
        for (int d = 20; d < 30; d ++) {
            string day;
            sstr << d;
            sstr >> day;
            sstr.clear();
            if (result[i].find(day) != result[i].end()) {
                for (int j = 0; j < result[i][day].num; j ++) {
                    fo << topics[i]->getId() << " " << result[i][day].tids[j] << " " <<
                        result[i][day].timestamp[j] << " test" << endl;
                    f2 << topics[i]->getId() << " " << result[i][day].tids[j]
                        << " " << 
                        result[i][day].relsim[j] << " " <<
                        result[i][day].novsim[i] << " " <<
                        result[i][day].timestamp[j] << " test" << " " <<
                        result[i][day].text[j] << endl;
                }
            }
        }
        delete topics[i];
        fo.close();
        f2.close();
    }
            /*
        pthread_t tids[params.size()];
        for (int i = 0; i < params.size(); i ++) {
            int ret = pthread_create(&tids[i], NULL, filterThread,
                    (void*)params[i]);
            cout << "current thread id = " << tids[i] << endl;
            if (ret != 0) {
                cout << "pthread create failed! error code = " << ret << endl;
            }
        }
        void * status;
        for (int i = 0; i < params.size(); i ++) {
            pthread_join(tids[i],&status);
        cout << "Done: query" << i << endl;
    }
    delete vidx; */
    return;
}

int main(int argc, char * argv[]) {
    
    map<string,string> conf = loadConf(argv[1]);
    double thr_rel = atof(argv[2]), thr_nov = atof(argv[3]);
    simFilter(conf, thr_rel, thr_nov);
    
    return 0;
}
