/*================================================
*   Copyright (C) 2015 All rights reserved.
* 
* file:   tweet.cpp
* author: Feiyue
* date:   2015/12/14
* brief:    
* 
* updated by: null
* 
*================================================*/
#include <vector>
#include <stdlib.h>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include "VirtualIndex.cpp"
#include "Utility.h"

#ifndef __TWEET_CPP__
#define __TWEET_CPP__

class tweet {
    private:
        unordered_map<string, double> vec;
        unordered_map<string, int> tf;
        int len;
        string time;
        string text;
        string id;
        time_t timestamp;
        string day;

    public:
        tweet(string _id, string _text, string _time) {
            id = _id;
            text = _text;
            time = _time;
            
            struct tm ts;
            sscanf(time.c_str(), "%*s%*s%d%d:%d:%d%*s%d", &ts.tm_mday,
                    &ts.tm_hour, &ts.tm_min, &ts.tm_sec, &ts.tm_year);
            ts.tm_mon = 6;
            ts.tm_year -= 1900;
            //cout << ts.tm_mday << " tm " << tmp << " "<<ts.tm_hour<< endl;
            timestamp = mktime(&ts) + 28800;
            stringstream sstr;
            sstr << ts.tm_mday;
            sstr >> day;
            
            vector<string> words = split(text, " ");
            len = words.size();
            for (int i = 0; i < words.size(); i ++) {
                if (vec.find(words[i]) != vec.end()) {
                    vec[words[i]] += 1.0 / (double) len;
                    tf[words[i]] += 1;
                }
                else {
                    vec[words[i]] = 1.0 / (double) len;
                    tf[words[i]] = 1;
                }
            }

        }
        int size() { return len; }
        unordered_map<string, double>&  getVec() { return vec; }
        unordered_map<string, int>& getTF() { return tf; }
        string getTime() { return time; }
        string getId() { return id; }
        string getDay() { return day; }
        string getText() { return text; }
        int getDim() { return vec.size(); }
        int getTimestamp() { return timestamp; }
        ~tweet() {
            unordered_map<string,double>().swap(vec); unordered_map<string, int>().swap(tf);
        }

};

#endif
