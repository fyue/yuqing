/*************************************************************************
Author: FeiYue
Created Time: 2012/7/4 17:56:38
File Name: query.cpp
Description: 
 ************************************************************************/
#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include "VirtualIndex.cpp"
#include "Utility.h"
#include "cluster.cpp"
using namespace std;

#ifndef __QUERY_CPP__
#define __QUERY_CPP__


class query
{
    private:
        vector<cluster> clusters;
        unordered_map<string, double> vec;
        int len;
        string text;
        string id;
    public:
        query(string _id, string _text)
        {
            string del = " ";     	
            id = _id;
            text = _text;
            unordered_map<string, double>().swap(vec);

            vector<string> words = split(text, del);
            len = words.size();
            int nw = words.size();
            for (int i = 0; i < nw; i ++) {
                if (vec.find(words[i]) == vec.end()) vec[words[i]] = 1.0 / (double)nw;
                else vec[words[i]] += 1.0 / (double) nw;
            }
        }
        
        int size() { return len;}
        void addNewCluster(tweet& t) {
            cluster cl(t);
            clusters.push_back(cl);
            /*map<string, double> cv = cl.getVec();
            cout << "new cluster" << endl;
            for (map<string, double>::iterator itr = cv.begin(); itr != cv.end(); itr++) {
                cout << itr->first << " " << itr->second << endl;
            }*/
            return;
        }
        void updateCluster(int idx, tweet &t) {
            clusters[idx].addTweet(t);
            return;
        }

        vector<cluster>& getClusters() {
            return clusters;
        }
        int getClusterNum() { return clusters.size();}
        unordered_map<string, double>& getVec() { return vec; }
        string getId() { return id; }
        string getText() { return text; }
        ~query() {
            unordered_map<string, double>().swap(vec);
        }
};

#endif
